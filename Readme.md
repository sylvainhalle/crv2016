BeepBeep Benchmark for the CRV 2016
===================================

This repository contains the source code for BeepBeep processor chains
evaluating monitoring properties from the 2016 [Competition on Runtime
Verification](http://crv.liflab.ca/wiki/index.php).

More information about the properties and their implementation can be found in
the following publication:

- S. Hallé. (2016). When RV Meets CEP. Proc. RV 2016, Springer LNCS 10012,
  68-91. DOI: 10.1007/978-3-319-46982-9_6

Since that publication, the source code of these properties has been updated
in order to follow the modifications and refactorings implemented in the
core BeepBeep library.

Building this project
---------------------

The project uses the standard structure of the
[AntRun](https://github.com/sylvainhalle/AntRun) build template.
To compile the project, make sure you have the following:

- The Java Development Kit (JDK) to compile. The palette complies
  with Java version 6; it is probably safe to use any later version.
- [Ant](http://ant.apache.org) to automate the compilation and build process

The palette also requires the following Java libraries:

- The latest version of [BeepBeep 3](https://liflab.github.io/beepbeep-3)
- The latest version of [BeepBeep's palettes](https://liflab.github.io/beepbeep-3-palettes);
  the project requires *Diagnostics*, *Fsm*, *Fol*, *Ltl* and *Xml*.

These three dependencies can be automatically downloaded and placed in the
`lib` folder of the project by typing:

    ant download-deps

From the project's root folder, the sources can then be compiled by simply
typing:

    ant

This will produce a file called `crv2016.jar` in the folder.

<!-- :maxLineLen=78: -->