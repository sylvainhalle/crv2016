package crv2016;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;


public class MapTest
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		Map<String,Object> map = new HashMap<String,Object>();
		Random r = new Random(0);
		for (int i = 0; i < 400000; i++)
		{
			String item_string = "item" + i;
			map.put(item_string, null);
			if (i > 0 && i % 1000 == 0)
			{
				long time_beg = System.nanoTime();
				for (int times = 0; times < 10; times++)
				{
					int index = r.nextInt(i);
					String lookup_string = "item" + index;
					map.containsKey(lookup_string);
				}
				long time_end = System.nanoTime();
				long duration_nanosecs = time_end - time_beg;
				System.out.printf("%d: %d\n", i, duration_nanosecs);
			}
		}

	}

}
