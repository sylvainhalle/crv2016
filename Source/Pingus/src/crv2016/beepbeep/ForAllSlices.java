/*
    BeepBeep processor chains for the CRV 2016
    Copyright (C) 2016-2020 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package crv2016.beepbeep;

import ca.uqac.lif.cep.Connector;
import ca.uqac.lif.cep.GroupProcessor;
import ca.uqac.lif.cep.Processor;
import ca.uqac.lif.cep.functions.ApplyFunction;
import ca.uqac.lif.cep.functions.Function;
import ca.uqac.lif.cep.functions.FunctionTree;
import ca.uqac.lif.cep.tmf.Slice;
import ca.uqac.lif.cep.util.Booleans;
import ca.uqac.lif.cep.util.Maps;

/**
 * Group processor that computes the conjunction of all the values
 * produced by each slice. It therefore acts as a universal quantifier
 * over slices.
 */
public class ForAllSlices extends GroupProcessor
{
  protected transient Processor m_processor;
  
  protected transient Function m_function;
  
  public ForAllSlices(Function slice_f, Processor p)
  {
    super(1, 1);
    m_function = slice_f;
    m_processor = p;
    Slice slice = new Slice(slice_f, p);
    ApplyFunction af = new ApplyFunction(new FunctionTree(Booleans.bagAnd, Maps.values));
    Connector.connect(slice, af);
    associateInput(0, slice, 0);
    associateOutput(0, af, 0);
    addProcessors(slice, af);
  }
  
  @Override
  public ForAllSlices duplicate(boolean with_state)
  {
    return new ForAllSlices(m_function.duplicate(with_state), m_processor.duplicate(with_state));
  }
}
