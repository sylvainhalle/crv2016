/*
    BeepBeep processor chains for the CRV 2016
    Copyright (C) 2016 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package crv2016.beepbeep;

import ca.uqac.lif.cep.Connector;
import ca.uqac.lif.cep.Connector.ConnectorException;
import ca.uqac.lif.cep.GroupProcessor;
import ca.uqac.lif.cep.fsm.FunctionTransition;
import ca.uqac.lif.cep.fsm.MooreMachine;
import ca.uqac.lif.cep.fsm.TransitionOtherwise;
import ca.uqac.lif.cep.functions.Constant;
import ca.uqac.lif.cep.util.Equals;
import ca.uqac.lif.cep.util.Lists;
import ca.uqac.lif.cep.functions.ApplyFunction;
import ca.uqac.lif.cep.functions.FunctionTree;
import ca.uqac.lif.cep.ltl.Troolean;
import ca.uqac.lif.cep.xml.XPathFunction;
import ca.uqac.lif.cep.xml.XPathFunctionGetText;

public class EndlessBashing extends GroupProcessor
{
	public EndlessBashing() throws ConnectorException
	{
		super(1, 1);
		ApplyFunction splitter = new ApplyFunction(new XPathFunction("message/characters/character"));
		addProcessor(splitter);
		Lists.Unpack dropper = new Lists.Unpack();
		addProcessor(dropper);
		Connector.connect(splitter, dropper);
		XPathFunctionGetText slicing = new XPathFunctionGetText("character/id/text()");
		ForAllSlices foas = null;
		foas = new ForAllSlices(slicing, new PinguFsm());
		addProcessor(foas);
		Connector.connect(dropper, foas);
		associateInput(0, splitter, 0);
		associateOutput(0, foas, 0);		
	}

	/**
	 * Moore machine describing the expected behaviour of a single Pingu
	 */
	protected static class PinguFsm extends MooreMachine
	{
		public PinguFsm()
		{
			super(1, 1);
			addTransition(0, new FunctionTransition(new FunctionTree(Equals.instance, new XPathFunctionGetText("character/status/text()"), new Constant("BASHER")), 1));
			addTransition(0, new TransitionOtherwise(0));
			addTransition(1, new FunctionTransition(new FunctionTree(Equals.instance, new XPathFunctionGetText("character/status/text()"), new Constant("BASHER")), 1));
			addTransition(1, new FunctionTransition(new FunctionTree(Equals.instance, new XPathFunctionGetText("character/status/text()"), new Constant("WALKER")), 0));
			addTransition(1, new TransitionOtherwise(2));
			addTransition(2, new TransitionOtherwise(2));
			addSymbol(0, new Constant(Troolean.Value.INCONCLUSIVE));
			addSymbol(1, new Constant(Troolean.Value.INCONCLUSIVE));
			addSymbol(2, new Constant(Troolean.Value.FALSE));
		}
		
		@Override
		public PinguFsm clone()
		{
			return new PinguFsm();
		}
	}
}