/*
    BeepBeep processor chains for the CRV 2016
    Copyright (C) 2016 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package crv2016.beepbeep;

import ca.uqac.lif.cep.Connector;
import ca.uqac.lif.cep.Connector.ConnectorException;
import ca.uqac.lif.cep.GroupProcessor;
import ca.uqac.lif.cep.tmf.WindowFunction;
import ca.uqac.lif.cep.util.Sets;
import ca.uqac.lif.cep.functions.ApplyFunction;
import ca.uqac.lif.cep.ltl.Always;
import ca.uqac.lif.cep.ltl.TrooleanCast;
import ca.uqac.lif.cep.xml.XPathFunction;

public class SpontaneousPinguCreation extends GroupProcessor
{
	public SpontaneousPinguCreation() throws ConnectorException
	{
		super(1, 1);
		ApplyFunction xpe = new ApplyFunction(new XPathFunction("message/characters/character/id/text()"));
		WindowFunction wf = new WindowFunction(Sets.isSupersetOrEqual);
		ApplyFunction cast = new ApplyFunction(TrooleanCast.instance);
		Always g = new Always();
		Connector.connect(xpe, wf, cast, g);
		addProcessors(xpe, wf, cast, g);
		associateInput(0, xpe, 0);
		associateOutput(0, g, 0);
	}
}
