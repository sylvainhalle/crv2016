/*
    BeepBeep processor chains for the CRV 2016
    Copyright (C) 2016-2020 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package crv2016.marq;

import ca.uqac.lif.cep.Connector;
import ca.uqac.lif.cep.Connector.ConnectorException;
import ca.uqac.lif.cep.GroupProcessor;
import ca.uqac.lif.cep.tmf.Slice;
import ca.uqac.lif.cep.util.Booleans;
import ca.uqac.lif.cep.fol.PredicateGet;
import ca.uqac.lif.cep.fol.PredicateGetNumber;
import ca.uqac.lif.cep.fol.PredicateTupleReader;
import ca.uqac.lif.cep.fsm.FunctionTransition;
import ca.uqac.lif.cep.fsm.MooreMachine;
import ca.uqac.lif.cep.fsm.TransitionOtherwise;
import ca.uqac.lif.cep.functions.ContextVariable;
import ca.uqac.lif.cep.functions.Constant;
import ca.uqac.lif.cep.functions.ContextAssignment;
import ca.uqac.lif.cep.util.Equals;
import ca.uqac.lif.cep.util.Numbers;
import ca.uqac.lif.cep.functions.Function;
import ca.uqac.lif.cep.functions.ApplyFunction;
import ca.uqac.lif.cep.functions.FunctionTree;
import ca.uqac.lif.cep.functions.StreamVariable;
import ca.uqac.lif.cep.ltl.Troolean;

public class Auction extends GroupProcessor
{
	public Slice slicer;
	
	public Auction() throws ConnectorException
	{
		super(1, 1);
		final int NO_AUCTION = 0;
		final int AUCTION_OPEN = 1;
		final int ERROR = 2;
		final int MIN_PRICE_REACHED = 3;
		final int FINISHED = 4;
		PredicateTupleReader reader = new PredicateTupleReader();
		MooreMachine item_machine = new MooreMachine(1, 1);
		// 0: endOfDay -> 0
		item_machine.addTransition(NO_AUCTION, new FunctionTransition(
				new FunctionTree(Equals.instance,
						new PredicateGet(0),
						new Constant("endOfDay")), NO_AUCTION));
		// 1: createAuction(_,X,Y) -> 1 / lastPrice := 0, minPrice := X, daysLeft := Y 
		item_machine.addTransition(NO_AUCTION, new FunctionTransition(
				new FunctionTree(Equals.instance,
						new PredicateGet(0),
						new Constant("create_auction")), AUCTION_OPEN,
				new ContextAssignment("lastPrice", new Constant(0)),
				new ContextAssignment("minPrice", new FunctionTree(Numbers.numberCast, new PredicateGet(2))),
				new ContextAssignment("daysLeft", new FunctionTree(Numbers.numberCast, new PredicateGet(3)))));
		item_machine.addTransition(NO_AUCTION, new TransitionOtherwise(ERROR));
		// 1: endOfDay & daysLeft = 1 -> 0
		item_machine.addTransition(AUCTION_OPEN, new FunctionTransition(
				new FunctionTree(Booleans.and,
						new FunctionTree(Equals.instance,
								new PredicateGet(0),
								new Constant("endOfDay")),
						new FunctionTree(Equals.instance,
								new ContextVariable("daysLeft"),
								new Constant(1f))), NO_AUCTION));
		// 1: endOfDay & daysLeft > 1 -> 1 / daysLeft := daysLeft - 1
		item_machine.addTransition(AUCTION_OPEN, new FunctionTransition(
				new FunctionTree(Booleans.and,
						new FunctionTree(Equals.instance,
								new PredicateGet(0),
								new Constant("endOfDay")),
								new FunctionTree(Numbers.isGreaterThan,
										new ContextVariable("daysLeft"),
										new Constant(1f))), AUCTION_OPEN,
						new ContextAssignment("daysLeft", new FunctionTree(Numbers.subtraction,
								new ContextVariable("daysLeft"), new Constant(1)))));
		// 1: bid(_X) & X < minPrice & x > lastPrice -> 1 / lastPrice := X
		item_machine.addTransition(AUCTION_OPEN, new FunctionTransition(
				new FunctionTree(Booleans.and,
						new FunctionTree(Equals.instance,
								new PredicateGet(0),
								new Constant("bid")),
						new FunctionTree(Booleans.and,
								new FunctionTree(Numbers.isGreaterThan,
										new PredicateGetNumber(2),
										new ContextVariable("lastPrice")),
								new FunctionTree(Numbers.isLessThan,
												new PredicateGetNumber(2),
												new ContextVariable("minPrice"))
										)), AUCTION_OPEN,
				new ContextAssignment("lastPrice", new PredicateGetNumber(2))));
		// 1: bid(_X) & X >= minPrice & x > lastPrice -> 3
		item_machine.addTransition(AUCTION_OPEN, new FunctionTransition(
				new FunctionTree(Booleans.and,
						new FunctionTree(Equals.instance,
								new PredicateGet(0),
								new Constant("bid")),
						new FunctionTree(Booleans.and,
								new FunctionTree(Numbers.isGreaterThan,
										new PredicateGetNumber(2),
										new ContextVariable("lastPrice")),
								new FunctionTree(Numbers.isGreaterThan,
												new PredicateGetNumber(2),
												new ContextVariable("minPrice"))
										)), MIN_PRICE_REACHED,
				new ContextAssignment("lastPrice", new PredicateGetNumber(2))));
		item_machine.addTransition(AUCTION_OPEN, new TransitionOtherwise(ERROR));
		item_machine.addTransition(ERROR, new TransitionOtherwise(ERROR));
		// 3: bid(_X) & X >= minPrice & x > lastPrice -> 3
		item_machine.addTransition(MIN_PRICE_REACHED, new FunctionTransition(
				new FunctionTree(Booleans.and,
						new FunctionTree(Equals.instance,
								new PredicateGet(0),
								new Constant("bid")),
						new FunctionTree(Booleans.and,
								new FunctionTree(Numbers.isGreaterThan,
										new PredicateGetNumber(2),
										new ContextVariable("lastPrice")),
								new FunctionTree(Numbers.isGreaterThan,
												new PredicateGetNumber(2),
												new ContextVariable("minPrice"))
										)), MIN_PRICE_REACHED,
				new ContextAssignment("lastPrice", new PredicateGetNumber(2))));
		// 3: sold -> 0
		item_machine.addTransition(MIN_PRICE_REACHED, new FunctionTransition(new FunctionTree(Equals.instance,
				new PredicateGet(0),
				new Constant("sold")), FINISHED));
		// 3: endOfDay -> 3
		item_machine.addTransition(MIN_PRICE_REACHED, new FunctionTransition(new FunctionTree(Equals.instance,
				new PredicateGet(0),
				new Constant("endOfDay")), MIN_PRICE_REACHED));
		item_machine.addTransition(MIN_PRICE_REACHED, new TransitionOtherwise(ERROR));
		item_machine.addTransition(FINISHED, new TransitionOtherwise(FINISHED));
		item_machine.addSymbol(NO_AUCTION, new Constant(Troolean.Value.TRUE));
		item_machine.addSymbol(AUCTION_OPEN, new Constant(Troolean.Value.TRUE));
		item_machine.addSymbol(ERROR, new Constant("false")); // A string is used to indicate cleanup
		item_machine.addSymbol(MIN_PRICE_REACHED, new Constant(Troolean.Value.FALSE)); // If price reached, must be sold
		item_machine.addSymbol(FINISHED, new Constant("true")); // A string is used to indicate cleanup
		Function cleanup = new FunctionTree(Booleans.or,
				new FunctionTree(Equals.instance, new Constant("true"), StreamVariable.X),
				new FunctionTree(Equals.instance, new Constant("false"), StreamVariable.X));
		Slice slicer = new Slice(new PredicateGet(1), item_machine, cleanup);
		Connector.connect(reader, slicer);
		ApplyFunction and = new ApplyFunction(Booleans.bagAnd);
		Connector.connect(slicer, and);
		addProcessors(reader, slicer, and);
		associateInput(0, reader, 0);
		associateOutput(0, and, 0);
		this.slicer = slicer;
	}

	
}
