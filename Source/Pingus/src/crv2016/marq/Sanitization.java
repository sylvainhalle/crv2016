/*
    BeepBeep processor chains for the CRV 2016
    Copyright (C) 2016 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package crv2016.marq;

import ca.uqac.lif.cep.GroupProcessor;
import ca.uqac.lif.cep.fsm.FunctionTransition;
import ca.uqac.lif.cep.fsm.MooreMachine;
import ca.uqac.lif.cep.fsm.TransitionOtherwise;
import ca.uqac.lif.cep.functions.ContextVariable;
import ca.uqac.lif.cep.functions.Constant;
import ca.uqac.lif.cep.functions.ContextAssignment;
import ca.uqac.lif.cep.util.Bags;
import ca.uqac.lif.cep.util.Booleans;
import ca.uqac.lif.cep.util.Equals;
import ca.uqac.lif.cep.functions.FunctionTree;
import ca.uqac.lif.cep.functions.StreamVariable;
import ca.uqac.lif.cep.util.Multiset;
import ca.uqac.lif.cep.util.Multiset.Insert;
import ca.uqac.lif.cep.util.NthElement;

public class Sanitization extends GroupProcessor 
{
	public Sanitization()
	{
		super(1, 1);
		MooreMachine m = new MooreMachine(1, 1);
		m.setContext("S", new Multiset());
		m.addTransition(0, new FunctionTransition(
				new FunctionTree(Equals.instance,
						new FunctionTree(new NthElement(0), StreamVariable.X),
						new Constant("sanitise")), 0,
						new ContextAssignment("S", new FunctionTree(Insert.instance,
								new ContextVariable("S"),
								new FunctionTree(new NthElement(1), StreamVariable.X)))));
		m.addTransition(0, new FunctionTransition(
				new FunctionTree(Booleans.and,
						new FunctionTree(Equals.instance,
								new FunctionTree(new NthElement(0), StreamVariable.X),
								new Constant("derive")),
								new FunctionTree(Bags.contains,
										new ContextVariable("S"),
										new FunctionTree(new NthElement(1), StreamVariable.X))), 0,
						new ContextAssignment("S", new FunctionTree(Insert.instance,
								new ContextVariable("S"),
									new FunctionTree(new NthElement(2), StreamVariable.X)))));
		m.addTransition(0, new FunctionTransition(
				new FunctionTree(Booleans.and,
						new FunctionTree(Equals.instance,
								new FunctionTree(new NthElement(0), StreamVariable.X),
								new Constant("use")),
								new FunctionTree(Booleans.not,
										new FunctionTree(Bags.contains,
												new ContextVariable("S"),
												new FunctionTree(new NthElement(1), StreamVariable.X)))), 1));
		m.addTransition(0, new TransitionOtherwise(0));
		m.addTransition(1, new TransitionOtherwise(1));
		m.addSymbol(0, new Constant(true));
		m.addSymbol(1, new Constant(false));
		associateInput(0, m, 0);
		associateOutput(0, m, 0);
	}
}
