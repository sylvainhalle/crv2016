package crv2016.marq;

import ca.uqac.lif.cep.Processor;
import ca.uqac.lif.cep.tmf.Source;
import ca.uqac.lif.synthia.Picker;
import ca.uqac.lif.synthia.random.RandomInteger;
import ca.uqac.lif.synthia.sequence.MarkovChain;
import ca.uqac.lif.synthia.util.Tick;
import java.util.Queue;

public class AuctionSource extends Source
{

  public AuctionSource()
  {
    super(1);
    
  }
  
  @Override
  protected boolean compute(Object[] inputs, Queue<Object[]> outputs)
  {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public Processor duplicate(boolean with_state)
  {
    // TODO Auto-generated method stub
    return null;
  }
  
  protected static class AuctionMarkovChain extends MarkovChain<Object[]>
  {
    /**
     * The item's ID
     */
    protected int m_id;
    
    public AuctionMarkovChain(int item_id, Picker<Float> picker)
    {
      super(picker);
      m_id = item_id;
      Tick price = new Tick(new RandomInteger(5, 100), new RandomInteger(1, 20));
      add(0);
    }
  }

}
